const http = require('http');

const port = 4000;
//create a variable "server" that stores the output of the creatServer
const server = http.createServer((request, response)=>{

	//to access the /greeting (routes) we will use the request object
	if(request.url === `/homepage`){
		response.writeHead(200, {'Content-Type': `text/plain`})
		response.end('This is the Home Page! Welcome!')
	} else if(request.url === `/about`){
		response.writeHead(200, {'Content-Type': `text/plain`})
		response.end('Welcome to about page')
	}else{
		response.writeHead(200, {'Content-Type': `text/plain`})
		response.end('Page not found, error 404')
	}
})

//use the server and port variable
server.listen(port);

//when server is running, print this:
console.log(`Server now accessible at localhost:${port}.`);


//1. Access 